package br.com.megaxhost.ex4;

public class CalculadoraDeComissao {

    

    public double calcular(Venda venda) {

        double valorFinal = venda.getQuantidadeVendida() * venda.getPrecoUnitario();
        double comissao = valorFinal * 0.05;

        return comissao;
    }

}
